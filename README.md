# First things to do after installing Axyl

What to do after a fresh install. This is just a reminder for me to easily set up a minimal arch installation

## 1. Installing Brave browser

```
paru -s brave-bin 
```

## 2. Installing Powerline for cool terminal effects

```
paru -s powerline 
```

## Add the following to  ~/.config/fish/config.fish to enable powerline in fish
set fish_function_path $fish_function_path "/usr/share/powerline/bindings/fish"
source /usr/share/powerline/bindings/fish/powerline-setup.fish
powerline-setup

## 3. Installing Spacevim 

```
curl -sLf https://spacevim.org/install.sh | bash
```




## Setting up git
```
cd existing_repo
git remote add origin https://gitlab.com/nilzon/first-things-to-do-after-installing-axyl.git
git branch -M main
git push -uf origin main
```
